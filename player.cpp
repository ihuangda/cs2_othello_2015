#include "player.h"
#include <vector>
#include <climits>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    current = new Board();
    me = side;
    if (me == BLACK)
    {
		enemy = WHITE;
	}
    else
    {
		enemy = BLACK;
	}
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

    //process opponents move
    if (opponentsMove != NULL)
    {
		current->doMove(opponentsMove, enemy);
	}
    Move *m;
    //do my move
    /*
   
    for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			m = new Move(i, j);
			if (current.checkMove(m, me))
			{
				current.doMove(m, me);
				return m;	
			}
			
		}
	}
	*/
	if (!(current->hasMoves(me)))
	{
		return NULL;
	}
	m = bestMove();
	current->doMove(m, me);
	return m;
	
}

/*
 * Calculates the best move for the side 
 */
Move *Player::bestMove()
{
	vector<Move *> moves = getMoves();
	Board *copy;
	int score = INT_MIN;
	Move *bestMove; 
	Move *m;
	for (unsigned int i = 0; i != moves.size(); i++)
	{
		copy = current->copy();
		m = moves[i];
		copy->doMove(m, me);
		int copy_score = copy->count(me);
		//prioritize corners
		if (m->getX() == 0 && m->getY() == 0)
		{
			copy_score *= 3;
		}
		else if (m->getX() == 7 && m->getY() == 7)
		{
			copy_score *= 3;
		}
		else if (m->getX() == 0 && m->getY() == 7)
		{
			copy_score *= 3;
		}
		else if (m->getX() == 7 && m->getY() == 0)
		{
			copy_score *= 3;
		}
		//deprioritize around corners
		else if (m->getX() - 1 == 0 && m->getY() - 1 == 0)
		{
			copy_score *= -3;
		}
		else if (m->getX() - 1 == 0 && m->getY() + 1 == 7)
		{
			copy_score *= -3;
		}
		else if (m->getX() + 1 == 7 && m->getY() - 1 == 0)
		{
			copy_score *= -3;
		}
		else if (m->getX() + 1 == 7 && m->getY() + 1 == 7)
		{
			copy_score *= -3;
		}
		
		
		if (copy_score > score)
		{
			score = copy_score;
			bestMove = moves[i];
		}
	}
	return bestMove;
}


/*
 * Gets all possible moves
 */
vector<Move *> Player::getMoves() {
	Move *m;
	vector<Move *> possibleMoves;
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			m = new Move(x, y);
			if (current->checkMove(m, me))
			{
				possibleMoves.push_back(m);
			}
		}
	}
	return possibleMoves;
}

